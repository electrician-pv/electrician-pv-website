---
hide:
  - navigation
  - toc
  - footer
  - feedback
---

!!! warning "Info"

    Wir arbeiten aktuell an dieser Seite.    
    Bilder von unserem Team folgen demnächst.

# Unser Team

<div class="grid cards" markdown>

-   :material-meter-electric:{ .lg .middle } __Frank Deischle__

    ---

    **Elektromeister**

<div style="margin:1px"></div>

</div>
<div class="grid cards" markdown>

-   :material-solar-panel:{ .lg .middle } __Mike Deischle__

    ---

    **Solarteur**

-   :material-solar-panel:{ .lg .middle } __Björn Kaufmann__

    ---

    **Solarteur**

-   :material-solar-panel:{ .lg .middle } __Robin Paul__

    ---

    **Solarteur**

-   :material-solar-panel:{ .lg .middle } __Johannes Niedzwiedz__

    ---

    **Solarteur**
</div>

<div style="margin:5px"></div>