---
hide:
  - navigation
  - toc
  - footer
  - feedback
---

# Stellenangebote bei electrician UG

## Wir sind immer auf der Suche nach neuen Talenten!

<div class="grid cards" markdown>

-   :material-plus-network:{ .lg .middle } __Jobs__

    ---

    Du hast Lust auf ein [tolles Team](/team) in einem zukunftsorientierten Job?    
    Hier findest du unsere attraktiven Stellenangebote.    
    Bewirb dich gerne [per Mail](mailto:info@electrician-pv.de) und wir freuen uns dich persönlich kennen zu lernen. 

    [Jetzt bewerben](mailto:info@electrician-pv.de){ .md-button }

</div>

<div class="grid cards" markdown>

-   :fontawesome-solid-helmet-safety:{ .lg .middle } __Montagehelfer/in (M/W/D)__

    ---
    
    **Deine Aufgaben:**  ![Montagehelfer](assets/FrankRegenbogen.jpg){ align=right width=40%} 

    - Installation/Montage (DC) von Solarmodulen auf Dächern    
    - AC-Kabel verlegen    

    **Das bringst du mit:**     

    - Lust auf das Thema Photovoltaik       
    - Selbständige Arbeitsweise und Zuverlässigkeit    
    - Führerschein Klasse B     

    **Das bieten wir dir:**    

    - Minijob oder Festanstellung    
    - Zukunftsorientiertes Berufsfeld    
    - Gute Verdienstmöglichkeiten    
    - Kollegiales und dynamisches Team 

-   :material-solar-power-variant:{ .lg .middle } __Elektriker/in (M/W/D)__

    ---

    **Deine Aufgaben:**  ![Elektriker](assets/Elektriker3.png){ align=right width=40%} 

    - Installation/Montage (DC) oder Inbetriebnahme und Abnahme (AC) von PV-Anlagen inkl. Speichersystemen und Zählerumbauten    
    - AC-Kabel verlegen

    **Das bringst du mit:**   

    - Abgeschlossene Ausbildung im Bereich der Elektrotechnik und Lust auf das Thema Photovoltaik       
    - Führerschein Klasse B      

    **Das bieten wir dir:**

    - Minijob oder Festanstellung    
    - Zukunftsorientiertes Berufsfeld    
    - Gute Verdienstmöglichkeiten    
    - Kollegiales und dynamisches Team    

</div>
