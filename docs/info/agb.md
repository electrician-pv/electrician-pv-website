---
hide:
  - feedback
---
# AGB

## Allgemeine Regelungen

#### Vertragsgegenstand

1. Die folgenden allgemeinen Geschäftsbedingungen regeln das Vertragsverhältnis zwischen der Firma Electrician UG (haftungsbeschränkt), Alter Holzweg 19, 38312 Dorstadt, und ihren Vertragspartnern.
Leistungen sowie Nachtragsaufträge werden ausschließlich auf der Grundlage nachfolgender Bedingungen ausgeführt. Die vorliegenden Bedingungen haben in jedem Fall Vorrang, auch wenn entgegenstehende Bedingungen des Kunden nicht ausdrücklich abgelehnt worden sind.

1. Dieser Vertrag ist ein Werkvertrag. Ergänzend zu den Regelungen dieses Vertrages finden die §631 ff. BGB Anwendung.

#### Vergütung

1. Bei Auftragserteilung ist eine Anzahlung von 20% der Vergütung zur Zahlung fällig. Die Restsumme ist nach Fertigstellung der Arbeiten sofort und ohne Abzug zur Zahlung fällig.

1. Die Auftragnehmerin kann den Beginn der Tätigkeit vom Eingang der Anzahlung abhängig machen.

#### Termine und Fristen

1. Ausführungstermin ist ca. 3 Wochen nach Einspeisezusage des Netzbetreibers. Über den Abschluss der Arbeiten wird der Auftraggeber benachrichtigt.

1. Werden die vereinbarten Fristen und Termine schuldhaft nicht eingehalten, so ist der jeweiligen Partei eine angemessene Frist zur Leistung zu setzen, nach Verstreichen der Nachfrist setzt ohne weitere Nachricht Verzug ein.

#### Mitwirkungspflichten des Auftraggebers

Der Auftraggeber ist zur Mitwirkung verpflichtet, soweit sich das aus den in diesem Vertrag und der Leistungsbeschreibung geregelten Pflichten ergibt, vor
allem Electrician UG (haftungsbeschränkt) oder deren Partnerunternehmen bei seiner Arbeit zu unterstützen. Er ist insbesondere verpflichtet, alle notwendigen Unterlagen rechtzeitig und unentgeltlich zur Verfügung zu stellen.

2. Er hat Electrician UG (haftungsbeschränkt) den Zugang zum Objekt zu ermöglichen.

3. Er ist verpflichtet, den Sachverständigen unverzüglich auf Änderungen hinzuweisen, die für seine Tätigkeit von Belang sind.

#### Abnahme

1. Die Abnahme der Vertragsleistung erfolgt nach Fertigstellung. Teilabnahmen finden nicht statt.

1. Über die Abnahme wird ein Protokoll erstellt, das von beiden Seiten zu unterzeichnen ist.

1. Ist die Leistung nicht vertragsgemäß und verweigert der Auftraggeber deshalb zu Recht die Abnahme oder erfolgt eine Abnahme unter Vorbehalt der Beseitigung von im Protokoll zu benennender Mängel, so ist die Auftragnehmerin verpflichtet, jeweils unverzüglich eine vertragsgemäße Leistung zu erbringen und die Mängel zu beseitigen, die voraussichtliche Dauer der Mängelbeseitigung mitzuteilen und nach Abschluss der Nacharbeiten die Mängelbeseitigung anzuzeigen.

#### Leistungsänderungen

1. Der Auftraggeber kann Änderungen von Inhalt und Umfang der Leistungen verlangen. Das gilt auch für bereits erbrachte und abgelieferte Teile.

1. Die Auftragnehmerin wird, wenn die Änderungen nicht nur unerheblich sind, die infolge der gewünschten Änderungen eintretenden Zeitverzögerungen und den Mehraufwand ermitteln und die Parteien werden sich über eine entsprechende Vertragsanpassung einigen. Finden die Parteien keine Einigung, so ist die Auftragnehmerin berechtigt, das Änderungsverlangen zurückzuweisen.

1. Mehrvergütungen für Leistungsänderungen, die der Auftraggeber nicht zu vertreten hat, kann die Auftragnehmerin nicht geltend machen.

1. Sämtliche Leistungsänderungen sind vor Beginn der Ausführung in einer schriftlichen Zusatzvereinbarung zu regeln, in der die zusätzliche Vergütung und etwaige Änderungen des Zeitablaufs festzuhalten sind.

#### Gewährleistung

Die Auftragnehmerin haftet für Sach- und Rechtsmängel nach den Regelungen des BGB für den Werkvertrag, der Auftraggeber hat aber zuerst die Rechte auf Nacherfüllung geltend zu machen. Schlägt diese fehl, stehen dem Auftraggeber die weiteren Mängelrechte (Selbstvornahme, Rücktritt, Minderung, Schadensersatz) zu.

#### Haftung

Die Auftragnehmerin haftet – außer bei Verletzung wesentlicher Vertragspflichten, bei der Verletzung von Leben, Körper oder Gesundheit oder bei Ansprüchen
aus dem Produkthaftungsgesetz- nur für Vorsatz und grobe Fahrlässigkeit. Wesentliche Vertragspflichten sind solche, deren Erfüllung zur Erreichung des Vertragszweckes notwendig ist.

#### Kündigung

Macht der Auftraggeber von seinem Kündigungsrecht nach 649 S. 1 BGB Gebrauch, kann die Auftragnehmerin als pauschale Vergütung 15 Prozent der vereinbarten Vergütung verlangen, wenn die Ausführung noch nicht begonnen hat. Hat die Ausführung schon begonnen, sind 80 Prozent der vereinbarten Vergütung zu zahlen.

#### Aufrechnung, Zurückbehaltungsrecht

1. Der Auftraggeber kann gegenüber den Forderungen des Auftragnehmers nur mit unbestrittenen oder rechtskräftig festgestellten Ansprüchen aufrechnen.

1. Der Auftraggeber darf ein Zurückbehaltungsrecht nur ausüben, wenn sein Gegenanspruch auf diesem Vertrag beruht.

#### Informationspflicht gemäß 36 VSBG

Die Auftragnehmerin beteiligt sich nicht an Verbraucherschlichtungsverfahren nach dem Verbraucherstreitbeilegungsgesetz.

#### Erfüllungsort, Gerichtsstand

1. Erfüllungsort für alle Verpflichtungen aus diesem Vertrag ist der Sitz der Auftragnehmerin.

1. Als ausschließlicher Gerichtsstand für Streitigkeiten aus und im Zusammenhang mit diesem Vertrag wird Amtsgericht Wolfenbüttel vereinbart.

#### Schlussvereinbarungen

1. Änderungen dieses Vertrags oder seiner Bestandteile bedürfen der Schriftform. Dies gilt auch für eine Änderung dieser Klausel. Mündliche Nebenabsprachen
sind unwirksam.

1. Für die Durchführung dieses Vertrags gilt ausnahmslos das Recht der Bundesrepublik Deutschland.

1. Sollte eine Bestimmung dieses Vertrags unwirksam sein oder werden, oder sollte der Vertrag eine Regelungslücke enthalten, so wird hierdurch die Wirksamkeit des Vertrags im Übrigen nicht berührt.
