---
hide:
  - navigation
  - toc
  - footer
---

# Versicherung

## Helvetia PV-Versicherung

Photovoltaikanlagen sind eine Investition in eine nachhaltige und umweltfreundliche Zukunft.     
Doch was passiert, wenn Ihre Anlage beschädigt wird oder gar komplett ausfällt?      
Eine Photovoltaikversicherung schützt Dich vor finanziellen Verlusten und sichert Deine Investition ab.

Unser Partner von Helvetia bietet Photovoltaikversicherungen mit umfassendem Schutz.

Weitere Informationen findest Du auf dem **[Helvetia Flyer](https://ebusiness.helvetia.com/de/content/dam/helvetia/de/helvetianet/de/komposit/Gesch%C3%A4ftskunden/Produktl%C3%B6sungen/Technische%20Versicherungen/Photovoltaik/Download-Center/Vertriebsinformation%20Photovoltaikversicherung.pdf)**.

---
![Helvetia](assets/helvetia_logo.png){: style="width:300px"}

**Yannick Zöller**    
Agenturleiter    
Gepr. Versicherungsfachmann IHK    

**Geschäftsstelle Braunschweig**    
Hamburger Str. 285, 38114 Braunschweig    

<a class="md-button" href="tel:+4915901086327" style="width:175px; text-align:center; bottom:0;">Mobil 
  <span class="twemoji"><svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M17 19H7V5h10m0-4H7c-1.11 0-2 .89-2 2v18a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2Z"></path></svg>
  </span>
</a>
<a class="md-button" href="mailto:yannick.zoeller@partner.helvetia.de" style="width:175px; text-align:center; bottom:0;">E-Mail 
<span class="twemoji">
  <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><!--! Font Awesome Free 6.2.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) Copyright 2022 Fonticons, Inc.--><path d="M498.1 5.6c10.1 7 15.4 19.1 13.5 31.2l-64 416c-1.5 9.7-7.4 18.2-16 23s-18.9 5.4-28 1.6l-126.3-52.5-40.1 74.5c-5.2 9.7-16.3 14.6-27 11.9S192 499 192 488v-96c0-5.3 1.8-10.5 5.1-14.7l165.3-212.6c2.5-7.1-6.5-14.3-13-8.4l-179 161.9-32 28.9c-9.2 8.3-22.3 10.6-33.8 5.8l-85-35.4C8.4 312.8.8 302.2.1 290s5.5-23.7 16.1-29.8l448-256c10.7-6.1 23.9-5.5 34 1.4z"></path></svg>
</span>
</a>
---
