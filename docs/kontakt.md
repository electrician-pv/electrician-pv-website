---
hide:
  - navigation
  - toc
  - footer
---

# Kontakt

## Haben wir dein Interesse geweckt?   

Über die E-Mail Adresse **[info@electrician-pv.de](mailto:info@electrician-pv.de)** kannst du Angebote für eine PV-Anlage, eine Erweiterung deiner bestehenden PV-Anlage, oder einen Batteriespeicher anfragen.

## Kontaktmöglichkeiten

Hier findest du alle Möglichkeiten uns zu kontaktieren.

<div class="grid cards" markdown>

-   :octicons-mail-16:{ .lg .middle } __Schreib uns eine E-Mail__

    ---

    Unsere Mitarbeiter werden deine Anfrage schnellstmöglich bearbeiten.

    [E-Mail :octicons-mail-16:](mailto:info@electrician-pv.de){ .md-button }

-   :simple-whatsapp:{ .lg .middle } __Tausch dich mit uns aus__

    ---

    Gerne chatten wir auch mit dir, melde dich einfach über WhatsApp.

    [WhatsApp :simple-whatsapp:](https://wa.me/message/BBHZO3DFBUSVO1){ .md-button }

-   :material-phone:{ .lg .middle } __Ruf uns an__

    ---

    Du möchtest uns lieber sprechen? Kein Problem ruf uns gerne an.

    [Telefon :material-phone:](tel:01728383121){ .md-button }

</div>

## Öffnungszeiten 

<div class="grid cards" markdown>

-   :fontawesome-solid-business-time:{ .lg .middle } __Öffnungszeiten__

    ---

    - [x] Montag - Freitag 09:00 Uhr – 17:00 Uhr    
    - [x] Samstag 10:00 Uhr – 18:00 Uhr    
    - [ ] Sonntag Geschlossen   

</div>

---
