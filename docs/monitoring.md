---
hide:
  - toc
  - footer
  - feedback
---

# Monitoring

<div class="monitor">
    <iframe width="1000" height="700" src="https://www.solarweb.com/PublicDisplay?token=af24d6e8-23ef-4ce9-a2c5-cf58927aff6e" frameborder="0" scrolling="no" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<div class="nomonitor">
    <h2>Leider kann das Monitoring nur im Querformat angezeigt werden.</h2> 
</div>

<div style="margin:5%"></div>