var feedback = document.forms.feedback
feedback.hidden = false 

feedback.style.paddingBottom = '50px';  
feedback.addEventListener("submit", function(ev) {
  ev.preventDefault()
  feedback.firstElementChild.disabled = true 
  var data = ev.submitter.getAttribute("data-md-value")
  console.log(data)
  if (data == 1) {  
    // Open the URL in a new tab  
    window.open('https://g.page/r/CfSRMpOr7gMWEAI/review', '_blank');  
  } else if (data == 2) {  
    // Open the second URL in a new tab  
    window.open('mailto:info@electrician-pv.de', '_blank');  
  } else if (data == 3) {  
    // Open the second URL in a new tab  
    window.open('https://wa.me/message/BBHZO3DFBUSVO1', '_blank');  
  } else if (data == 4) {  
    // Open the second URL in a new tab  
    window.open('https://www.instagram.com/electrician_pv_team/', '_blank');  
  } 
})
