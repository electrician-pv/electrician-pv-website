---
hide:
  - footer
---

# Referenzen

## Hier findest du einige unserer Projekte.

---
### Flachdach

<div class="refcont">
  <div class="refimg">
    <img src="../../assets/references/FlachDachPlan.png" alt="Avatar" style="width: 100%; margin-bottom: -20px;">
    <p>Planungsansicht</p>
  </div>
  <div class="refimg">
    <img src="../../assets/references/FlachDach.png" alt="Avatar" style="width: 100%; margin-bottom: -20px;">
    <p>Aufbauansicht</p>
  </div>
</div>

Hier zu sehen ist ein Nord/Süd ausgerichtetes Garagendach mit 11° Neigung.    
Verbaut wurden 26 <a class="underline" href="https://www.solarwatt.de/canto/download/pkpkpcueol4gnb7mmi592jm008" target="_blank" rel="noopener">Solarwatt classic pure 405Wp</a> Panels

| Übersicht | <div style="width:10vw"></div> |
| --- | --- |
|PV-Generatorfläche |	50,4m² |
|PV-Generatorleistung |	10,53kWp |
|PV-Generatorenergie (AC-Netz) |	9.555kWh/Jahr |
|Vermiedene CO2-Emissionen |	4.470 kg/Jahr |

---
### Hausdach 1

<div class="refcont">
  <div class="refimg">
    <img src="../../assets/references/Dach1.png" alt="Avatar" style="width: 100%; margin-bottom: -20px;">
    <p>Planungsansicht</p>
  </div>
  <div class="refimg">
    <img src="../../assets/references/Dach1Plan.png" alt="Avatar" style="width: 100%; margin-bottom: -20px;">
    <p>Aufbauansicht</p>
  </div>
</div>

Bei diesem südlich ausgerichteten Dach mit 38° Neigung wurden 26 <a class="underline" href="https://www.solarwatt.de/canto/download/a0lbnvq42d5l3c0lbdcn30n92s" target="_blank" rel="noopener">375Wp Solarwatt Panels</a> verbaut.     
Als Wechselrichter kam der <a class="underline" href="https://www.fronius.com/de-de/germany/solarenergie/installateure-partner/technische-daten/alle-produkte/wechselrichter/fronius-symo-gen24-plus/fronius-symo-gen24-8-0-plus" target="_blank" rel="noopener">Symo GEN24 8.0 Plus</a> zum Einsatz.

| Übersicht| <div style="width:10vw"></div> |
| --- | --- |
|PV-Generatorfläche |	50,4m² |
|PV-Generatorleistung |	10,53kWp |
|PV-Generatorenergie (AC-Netz) |	9.555kWh/Jahr |
|Vermiedene CO2-Emissionen |	4.470 kg/Jahr |

---
### Hausdach 2

<div class="refcont">
  <div class="refimg">
    <img src="../../assets/references/Dach2Plan.png" alt="Avatar" style="width: 100%; margin-bottom: -20px;">
    <p>Planungsansicht</p>
  </div>
  <div class="refimg">
    <img src="../../assets/references/Dach2.png" alt="Avatar" style="width: 100%; margin-bottom: -20px;">
    <p>Aufbauansicht</p>
  </div>
</div>

Ein weiteres Beispiel ist dieses südlich ausgerichtete Dach mit 45° Neigung.    
Hier wurden 23 <a class="underline" href="https://www.solarwatt.de/canto/download/a0lbnvq42d5l3c0lbdcn30n92s" target="_blank" rel="noopener">375Wp Solarwatt Panels</a> verbaut sowie der <a class="underline" href="https://www.fronius.com/de-de/germany/solarenergie/installateure-partner/technische-daten/alle-produkte/wechselrichter/fronius-symo-gen24-plus/fronius-symo-gen24-8-0-plus" target="_blank" rel="noopener">Symo GEN24 8.0 Plus</a> Wechselrichter

| Übersicht | <div style="width:10vw"></div> |
| --- | --- |
|PV-Generatorfläche |	50,4m² |
|PV-Generatorleistung |	8,63kWp |
|PV-Generatorenergie (AC-Netz) |	9.465kWh/Jahr |
|Vermiedene CO2-Emissionen |	4.427 kg/Jahr |

---