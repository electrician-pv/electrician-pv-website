---
hide:
  - toc
  - footer
---

# Partnerschaften

## Hersteller

![Fronius](assets/fronius_system_partner_logo.png){: style="width:400px"}    

![Solarwatt](assets/solarwatt_logo.png){: style="width:400px"}

---