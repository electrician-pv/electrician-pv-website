from http import client
import logging
import boto3
import os
import sys

def main():
    try:
        aws_access_key = os.environ["AWS_ACCESS_KEY"]
        aws_secret_key = os.environ["AWS_SECRET_KEY"]
    except KeyError as e:
        print("Environment variable not found:", e)
        sys.exit(1)
    
    client = boto3.client('s3', 
    aws_access_key_id = aws_access_key,
    aws_secret_access_key = aws_secret_key,
    )
    
    client.upload_file(
        'site/index.html', 
        'electrician-pv.de', 
        'index.html',
        ExtraArgs={'ACL': 'public-read', 'ContentType': 'html'})
    
    uploadDirectory('./site/')
    
    
def uploadDirectory(path):
    for root,dirs,files in os.walk(path):
        #for file in files:
        #    print(file)
        for dir in dirs:
            print(dir)
    
if __name__ == "__main__":
    main()
