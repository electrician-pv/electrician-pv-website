# electrician-pv-website

Used to store website generator data of [https://electrician-pv.de/](https://electrician-pv.de/)

# Links

[AWS Console Home](https://console.aws.amazon.com/console/home)    
[Website Electrician](https://electrician-pv.de)    
[MkDocs-Material](https://squidfunk.github.io/mkdocs-material)    
[GitLab Repo](https://gitlab.com/electrician-pv/electrician-pv-website)    

# Contact

Mail: [info@electrician-pv.de](mailto:info@electrician-pv.de)
